Config:

    config file is json.
    use_proxy = 1 to use proxies from main proxy.txt
    data_to_parse is list of triplets (name, xpath, count), count = 0 means take all.

    {
        "start_urls": [ "http://cte.ibsu.edu.ge/en" ],
        "domains": [ "cte.ibsu.edu.ge" ],
        "save_file": "cte.json",
        "thread_number": 16,
        "data_to_parse": [('title', '//title/text()', 1)],
        "consider_robot" : 1,
        "use_proxy": 1
    }



Graph:

    copy paste graph.txt to https://dreampuf.github.io/GraphvizOnline/.

    Engine: circo. Format: svg.

    download svg by clicking on image.

    veiw svg online https://vectorpaint.yaks.co.nz/.
 


ToDo:

    apply proxy api get list of proxies and if link fails try using some other proxies.
        Implement "proxy sleeping period". (use certain proxy every X seconds.)

    link filters by regex. 

    jsbased pages.

