from spider import Spider
import queue, os, threading, sys, json, time, requests

class Crawler:
    def __init__(self, start_urls, domains, save_file = "data.json", thread_number = 1, data_to_parse = [], consider_robot = True, use_proxy = False):
        self.start_urls = start_urls
        self.domains = domains
        self.consider_robot = consider_robot
        self.save_file = save_file
        self.data_to_parse = data_to_parse
        self.thread_number = thread_number
        self.lock_queue = threading.Lock()
        self.lock_ofile = threading.Lock()
        self.count_parsed_pages = 0
        
        if use_proxy:
            self.proxies = open(os.path.dirname(os.path.realpath(__file__)) + "/../proxy.txt","r").read().split()
            print(self.proxies)

        self.url_from = {}
        self.q = queue.Queue()
        for url in self.start_urls:
            self.q.put(url)
            self.url_from[url] = "START"        
        
    

    def start(self):
        if os.path.exists(self.save_file):
            os.remove(self.save_file)

        self.spider = Spider(crawler = self, name="spider")
        self.spider.start(thread_number = self.thread_number)
        
        with open(self.save_file, "a+") as f:
            f.write("\n]")




if __name__ == "__main__":

    _config = open(sys.argv[1], "r").read()
    _config = json.loads(_config)

    c = Crawler(**_config)

    start_ctime = time.ctime()
    start_time = time.time()

    try:
        c.start()
    except:
        print("Error")
    
    end_time = time.time()

    print("time: {0:.2f} seconds\npages: {1}\n".format(end_time - start_time, c.count_parsed_pages))

    stat = "{0} - {1}.  time: {2:.2f} seconds.  pages: {3}.\n".format(
        " ".join(sys.argv),
        start_ctime,
        end_time - start_time,
        c.count_parsed_pages)
    open("time.txt", "a").write(stat)


    

    