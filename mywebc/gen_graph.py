import json,os
json_file =  open(os.path.dirname(os.path.realpath(__file__)) + "/cte.json", "r")


def filter(x):
    a = ""
    last = 0
    for i in x:
        if i.isalnum():
            a += i
            last = 0
        else:
            if last == 0:
                a += "_"
                last = 1
    return a
    

f = open(os.path.dirname(os.path.realpath(__file__)) +"/graph.txt","w")

f.write("""
digraph G {
node [style=filled,width=2,height=2];
""")

for data in json.load(json_file):
    x,y = filter(data["from"]), filter(data['url'])
    f.write("{0} -> {1};\n".format(x,y))

f.write("}")