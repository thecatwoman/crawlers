import requests
from lxml import html
from urllib.parse import urljoin, urlunparse, urlparse
import os
import json
import urllib.robotparser
import threading
import random 


class Spider:
    def __init__(self, crawler = None, name = "spider"):
        self.name = name
        self.crawler = crawler
        self.robot_test = None
        self.threads = {}
        if self.crawler.consider_robot:
            self.robot_test = {}
            for d in self.crawler.domains:
                self.robot_test[d] = urllib.robotparser.RobotFileParser()
                self.robot_test[d].set_url(urlunparse(("http", d, "robots.txt","","","")))
                self.robot_test[d].read()
    

    def start(self, thread_number = 1):
        all_threads_done = False
        while not all_threads_done:
            all_threads_done = True
            for ind in range(thread_number):
                thread = self.threads.get(ind)
                if thread == None or not thread.is_alive():
                    if not self.crawler.q.empty():
                        url = self.crawler.q.get()
                        self.threads[ind] = threading.Thread(target=self.crawl_url, args=(url,ind))
                        self.threads[ind].start()
                        all_threads_done = False
                else:
                    all_threads_done = False
    
    
    def crawl_url(self, url, ind):
        # try 3 times to download page
        for _ in range(3):
            try:
                req = {
                    'url': url,
                    'headers': {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'},
                }

                if len(self.crawler.proxies) > 0:
                    rand_proxy = random.choice(self.crawler.proxies)
                    req['proxies'] = { "http" : rand_proxy, "https" : rand_proxy }
                
                # check if url is website
                r = requests.head(**req)
                if "text/html" not in r.headers["content-type"]:
                    print("Not website:", url)
                    return

                r = requests.get(**req)

                links = self.parse(response=r, url=url)

                with self.crawler.lock_queue:
                    for link in links:
                        if self.crawler.url_from.get(link) == None:
                            self.crawler.url_from[link] = url
                            self.crawler.q.put(link)
                break
            except:
                pass


    def parse(self, response, url):
        if response.status_code != 200:
            return []
        
        tree = html.fromstring(response.content)
        
        self.parse_data(response=response, tree=tree, url=url)
        
        links = tree.xpath('//a/@href')

        links = [urljoin(response.url, l) for l in links]
        #domain filter
        links = [l for l in links if any(d in urlparse(l).netloc for d in self.crawler.domains)]
        #http and extra symbolos
        links = [l.replace("https://","http://").rstrip("/?#") for l in links ]
        #robot filter
        if self.crawler.consider_robot:
            links = [l for l in links if any(self.robot_test[d].can_fetch("*", l) for d in self.crawler.domains)]
        return links
    

    def parse_data(self, response, tree, url):
        with self.crawler.lock_queue:
            url_from = self.crawler.url_from[url]

        data = {
            'from' : url_from,
            'url' : response.url,
        }

        for name, xpath, count in self.crawler.data_to_parse:
            d = tree.xpath(xpath)
            if count == 0:
                count = len(d)
            data[name] = d[:count]

        # save data
        with self.crawler.lock_ofile:
            self.crawler.count_parsed_pages += 1
            print(self.crawler.count_parsed_pages, url)
            if not os.path.exists(self.crawler.save_file):
                with open(self.crawler.save_file, "w") as f:
                    f.write("[\n\t{0}".format(json.dumps(data)))
            else:
                with open(self.crawler.save_file, "a+") as f:
                    f.write(",\n\t{0}".format(json.dumps(data)))
        


