# -*- coding: utf-8 -*-
import scrapy, random, os
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule


class ProxyProvider:
    def __init__(self):
        self.proxies = open(os.path.dirname(os.path.realpath(__file__)) + "/../../../proxy.txt","r").read().split()
        print(self.proxies)

    def get_proxy(self):
        return random.choice(self.proxies)

pp = ProxyProvider()

class MainSpider(CrawlSpider):
    name = 'main'
    
    allowed_domains = ['cte.ibsu.edu.ge']
    start_urls = ['http://cte.ibsu.edu.ge/en']

    rules = (
        # Rule(LinkExtractor(restrict_xpaths="//a[contains(@href, 'seu.edu.ge/ka/')]"), callback='parse_item', follow=True), 
        Rule(LinkExtractor(), callback='parse_item', follow=True, process_request='process_request'),
    )

    def process_request(self, request):
        # request.meta['proxy'] = "http://" + pp.get_proxy()
        return request

    def parse_item(self, response):
        item = {
            'url' : response.url,
            'title' : response.xpath('//title/text()').get(),
            # 'links' : [l.url for l in LinkExtractor(unique=True).extract_links(response)]
        }
        return item
